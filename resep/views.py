from django.shortcuts import render


def resep(request):
    judul = ["Nasi Goreng", "Gado Gado", "Ayam Rica Rica", "Rendang", "Gulai Ikan Patin"]
    penulis = "Nailil Falahah"

    konteks = {
        'title': judul,
        'penulis': penulis,

    }

    return render(request, 'resep.html', konteks)
