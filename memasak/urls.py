from django.contrib import admin
from django.urls import path
from resep.views import resep

urlpatterns = [
    path('admin/', admin.site.urls),
    path('resep/', resep),
]
